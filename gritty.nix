{ haskellPackages, lib }:

let
  name = "gritty";
  proj = haskellPackages.callCabal2nix "${name}" (lib.cleanSource ./.) {};
in
  proj.overrideAttrs (old:
    { postBuild = ''
      mkdir -p $out/share/zsh/vendor-completions
      mkdir -p $out/share/bash-completion/completions
      dist/build/${name}/${name} --zsh-completion-script "$out/bin/${name}" > $out/share/zsh/vendor-completions/_${name}
    #   dist/build/${name}/${name} --bash-completion-script "$out/bin/${name}" > $out/share/bash-completion/completions/${name}
    #   '';
    })
