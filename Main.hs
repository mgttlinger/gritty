{-# LANGUAGE OverloadedStrings, OverloadedLabels, RecordWildCards #-}

module Main where

import qualified GI.Gtk as Gtk
import Data.GI.Base
import Data.GI.Base.GType

import Data.Char
import Data.List.Index
import Data.List
import Data.List.Split
import Data.Maybe
import qualified Data.Text as T

import System.IO

import TextShow

import Options.Applicative

data Params = Params { limit :: Int -- ^ Limit the columns to the given number. Columns are joined if necessary to reduce to this number.
                     , delimiter :: Char -> Bool -- ^ Function to determine splitting points.
                     , separator :: String -- ^ Separator used when joining columns over the limit
                     }

-- | Generates a delimiter value from a String.
parseSep :: String -> (Char -> Bool)
parseSep "\\s" = isSpace
parseSep s = flip elem s

-- | Cmd argument parser and documentation
params :: Parser Params
params = Params
  <$> option auto (  long "limit"
                  <> short 'l'
                  <> value 100
                  <> help "Maximum number of columns to split"
                  <> showDefault
                  )
  <*> (fmap parseSep $ option auto (  long "delimiter"
                                   <> short 'd'
                                   <> value "\\s"
                                   <> help "Splitting character classes"
                                   ))
  <*> strOption (  long "separator"
                <> short 's'
                <> value "\\t"
                <> showDefault
                <> help "Separator for columns over the limit"
                )

args = info (params <**> helper) (fullDesc
                                  <> progDesc "Display pipe in a sortable GUI table"
                                  <> header "gritty - the GTK Out-GridView replacement")

-- | Truncate the row to the given number of columns by joining the truncated ones into the last.
flatten :: Params -> [String] -> [String]
flatten p@Params{..} ss | limit <= 1 = [intercalate separator ss]
                        | length ss < 1 = []
                        | otherwise = head ss : (flatten (p { limit = limit - 1}) $ tail ss)

-- | Split a line of text into columns as parametrised.
tabulateLine :: Params -> String -> [String]
tabulateLine p@Params{..} = (flatten p) . wordsBy delimiter

-- | Split a pipe linewise into a table.
tabulate :: Params -> [String] -> [[String]]
tabulate p = fmap $ tabulateLine p


addCell tv pos = do
  c <- new Gtk.TreeViewColumn [ #title := showt pos
                              , #reorderable := True
                              , #resizable := True
                              , #visible := True
                              , #sortColumnId := fromIntegral pos - 1
                              ]
  cel <- Gtk.cellRendererTextNew
  Gtk.cellLayoutPackStart c cel True
  Gtk.cellLayoutSetCellDataFunc c cel $ Just (\ lay ren mo it -> do
                                                 gdat <- Gtk.treeModelGetValue mo it (fromIntegral pos - 1)
                                                 dat <- fromGValue gdat :: IO (Maybe String)
                                                 mcr <- castTo Gtk.CellRendererText ren
                                                 maybe (pure ()) (\ cr -> set cr [ #text := T.pack $ fromMaybe "" dat ]) mcr)

  Gtk.treeViewAppendColumn tv c

putRow ls dat = do
  it <- Gtk.listStoreAppend ls
  itraverse (\ i v -> Gtk.listStoreSetValue ls it (fromIntegral i) =<< toGValue v) dat

-- | Read StdIn until EOF.
readAll :: IO [String]
readAll = do
  done <- isEOF
  if done
    then pure []
    else do cnt <- getLine
            rest <- readAll
            return $ rest <> [cnt]

main :: IO ()
main = do
  pars <- execParser args
  dat <- tabulate pars . reverse <$> readAll
  let len = maximum (length <$> dat)

  Gtk.init Nothing

  win <- new Gtk.Window [ #title := "Gritty"
                        ]

  on win #destroy Gtk.mainQuit

  ls <- Gtk.listStoreNew $ take len $ repeat gtypeString
  traverse (putRow ls) $ fmap (Just :: String -> Maybe String) <$> dat

  table <- Gtk.treeViewNewWithModel ls
  set table [ #enableGridLines := Gtk.TreeViewGridLinesBoth
            ]

  traverse (\ i -> addCell table i) $ [1 .. len]

  #add win table

  #showAll win

  Gtk.main
